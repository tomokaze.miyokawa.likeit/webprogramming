package model;

import java.io.Serializable;
import java.sql.Date;


public class User implements Serializable {
	private int id;
	private String loginId;
	private String name;
	private Date birthDate;
	private String password;
	private String createDate;
	private String upDate;


	public User(String loginId, String name) {
		this.loginId = loginId;
		this.name = name;
	}

	public User(int id, String loginId, String name, Date birthDate, String password, String createDate, String upDate) {
		this.setId(id);
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.password = password;
		this.createDate = createDate;
		this.upDate = upDate;


	}
	public String getLoginId() {
		return loginId;
	}
	public void setLognId(String lognId) {
		this.loginId = lognId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getUpDate() {
		return upDate;
	}
	public void setUpDate(String upDate) {
		this.upDate = upDate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}






}
