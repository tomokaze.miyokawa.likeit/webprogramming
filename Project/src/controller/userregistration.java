package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;

/**
 * Servlet implementation class userregistration
 */
@WebServlet("/userregistration")
public class userregistration extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public userregistration() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_registration.jsp");
		dispatcher.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String loginid = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String birthdate = request.getParameter("birthdate");

		UserDao userDao = new UserDao();
		String loginId2 = userDao.findByLoginId(loginid);



		if(loginId2!=null) {

			request.setAttribute("errMsg", "入力された内容は正しくありません。1");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_registration.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if(!password.equals(password2)) {

			request.setAttribute("errMsg", "入力された内容は正しくありません。2");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_registration.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if(loginid.equals("") || password.equals("") || password2.equals("") || name.equals("") || birthdate.equals("")) {

			request.setAttribute("errMsg", "入力された内容は正しくありません。3");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_registration.jsp");
			dispatcher.forward(request, response);
			return;
		}
		
		

		String pass =userDao.code(password);

		userDao.registration(loginid,pass,name,birthdate);
		
		



		HttpSession session = request.getSession();
		session.setAttribute("userInfo", userDao);


		response.sendRedirect("userlistservlet");

	}

}
