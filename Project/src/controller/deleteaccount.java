package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class deleteaccount
 */
@WebServlet("/deleteaccount")
public class deleteaccount extends HttpServlet {
	private static final long serialVersionUID = 1L;


    public deleteaccount() {
        super();

    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String id = request.getParameter("id");


		UserDao userDao = new UserDao();
		User user = userDao.information(id);

		
		request.setAttribute("user", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/delete_account.jsp");
		dispatcher.forward(request, response);
	}



	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");

		UserDao userDao = new UserDao();

		userDao.delete(id);


		HttpSession session = request.getSession();
		session.setAttribute("userInfo", userDao);


		response.sendRedirect("userlistservlet");

	}

}
