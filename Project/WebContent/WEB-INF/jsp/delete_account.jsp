<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ユーザー削除</title>
    <link rel="stylesheet"  	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"               integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="css/delete_account.css" rel="stylesheet">
</head>
<body>
<form action="deleteaccount" method="post">

    <header><span class="user-name">ユーザー名さん</span>
        <a href="loginservlet">ログアウト</a>
    </header>
    <h1>ユーザ削除確認</h1>


    <h2>ログインid:${user.loginId}
        <br>
        を本当に削除してよろしいでしょうか。</h2>
        <input type="hidden" name="id" value="${user.id}">

    <div class="button-area">
    <a href="userlistservlet">キャンセル</a>　　　　　<button type="submit" class="btn btn-secondary">OK</button>
        </div>
</form>
</body>
</html>