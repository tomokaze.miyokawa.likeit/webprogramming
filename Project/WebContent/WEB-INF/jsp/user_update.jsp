<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ユーザ情報更新</title>
    <link rel="stylesheet"  	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"               integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="css/user_update.css" rel="stylesheet">
</head>
<body>
    <form method="post" action="userupdate">

    <header><span class="user-name">ユーザー名さん</span>
        <a href="loginservlet">ログアウト</a>
    </header>
    <h1>ユーザ情報更新</h1>

            	<c:if test="${errMsg != null}">
		  ${errMsg}
	</c:if>


    <h2>ログインID　　　　　${user.loginId}</h2>
    <input type="hidden" name="loginId" value="${user.loginId}">
    <h2>パスワード　　　　　<input type="password" name="password"></h2>
    <h2>パスワード(確認)　　　　　<input type="password" name="password2"></h2>
    <h2>ユーザ名　　　　　<input type="text" name="name" value="${user.name}"></h2>
    <h2>生年月日　　　　　<input type="date" name="birthdate" value="${user.birthDate}"></h2>


    <div class="button-area">
    <button type="submit" class="btn btn-secondary">更新</button>
        </div>

    <p><a href="userlistservlet">戻る</a></p>

</form>
</body>
</html>