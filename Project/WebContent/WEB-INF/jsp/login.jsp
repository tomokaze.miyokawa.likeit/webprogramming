<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ログイン画面</title>
    <link rel="stylesheet"  href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="css/login.css" rel="stylesheet">
</head>
<body>
    <form action="loginservlet" method="post">

    	<c:if test="${errMsg != null}" >
		  ${errMsg}
	</c:if>

    <h1>ログイン画面</h1>
    <h2>ログインID<input type="text" name="loginId"></h2>

    <h2>パスワード<input type="text" name="password"></h2>

    <div class="button-area">
    <button type="submit" class="btn btn-secondary">ログイン</button>
        </div>
        </form>
</body>
</html>