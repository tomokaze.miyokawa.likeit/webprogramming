<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ユーザ情報詳細参照</title>
    <link rel="stylesheet"  	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"               integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="css/user_information.css" rel="stylesheet">
</head>
<body>
    <header><span class="user-name">ユーザー名さん</span>
        <a href="loginservlet">ログアウト</a>
    </header>
    <h1>ユーザ情報詳細参照</h1>


    <h2>ログインID　　　　　${user.loginId}</h2>
    <h2>ユーザ名　　　　　　${user.name}</h2>
    <h2>生年月日　　　　　　${user.birthDate}</h2>
    <h2>登録日時　　　　　　${user.createDate}</h2>
    <h2>更新日時　　　　　　${user.upDate}</h2>

    <p><a href="userlistservlet">戻る</a></p>

</body>
</html>