<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ユーザ新規登録</title>
    <link rel="stylesheet"  	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"               integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="css/user_registration.css" rel="stylesheet">
</head>
<body>
    

    	<c:if test="${errMsg != null}" >
		  ${errMsg}
	</c:if>

    <header><span class="user-name">ユーザー名さん</span>
        <a href="loginservlet">ログアウト</a>
    </header>
    <h1>ユーザ新規登録</h1>
    
    <form method="post" action="userregistration">
    <h2>ログインID<input type="text" name="loginId"></h2>
    <h2>パスワード<input type="text" name="password"></h2>
    <h2>パスワード(確認)<input type="text" name="password2"></h2>
    <h2>ユーザ名<input type="text" name="name"></h2>
    <h2>生年月日<input type="Date" name="birthdate"></h2>

    <div class="button-area">
    <button type="submit" class="btn btn-secondary">登録</button>
        </div>
        </form>
    <p><a href="userlistservlet">戻る</a></p>


</body>
</html>