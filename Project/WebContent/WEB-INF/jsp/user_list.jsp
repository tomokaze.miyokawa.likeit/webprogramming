<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ユーザー一覧</title>
    <link rel="stylesheet"  	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"               integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="css/user_list.css" rel="stylesheet">
</head>
<body>
    
    <header><span class="user-name">ユーザー名さん</span>
        <a href="loginservlet">ログアウト</a>
    </header>
    <h1>ユーザ一覧</h1>

    <h2><a href="userregistration">新規登録</a></h2>
    <form action="userlistservlet" method="post">
    <h2>ログインID<input type="text" name="loginId"></h2>
    <h2>ユーザー名<input type="text" name="name"></h2>
    <h2>生年月日<input type="Date" name="fromBirthDate">〜<input type="Date" name="toBirthDate"></h2>

    <div class="button-area">
    <button type="submit" class="btn btn-secondary">検索</button>
        </div>
        </form>
    <hr>
        <table border="1" align="center">
    <tr>
      <th>ログインID</th>
      <th>ユーザー名</th>
      <th>生年月日</th>
      <th></th>
    </tr>
    <c:forEach var="user" items="${userList}">
    <tr>
     <td>${user.loginId}</td>
     <td>${user.name}</td>
     <td>${user.birthDate}</td>
     <td>
             <a href="userinformation?id=${user.id}" class="badge badge-primary">詳細</a>
        <c:if test= "${userInfo.loginId == 'admin' || userInfo.loginId == user.loginId}">
        <a href="userupdate?id=${user.id}" class="badge badge-success">更新</a></c:if>
        <c:if test= "${userInfo.loginId == 'admin'}">
        <a href="deleteaccount?id=${user.id}" class="badge badge-danger">削除</a></c:if>
        </td>
        </tr>
    </c:forEach>

    </table>

</body>
</html>